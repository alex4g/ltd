package w2;

public class W3t27_50 {

	public static void main(String[] args) {
		
//		System.out.println(t28(2,4));
//		
//		System.out.println(t30(2.0,3));		
//		
//		System.out.println(t31(7));
//
//		System.out.println(toString(t32(10)));
//		
//		System.out.println(toString(t33(1.3)));
//		
//		t34(2235);
//				
//		t35(3);
//		
//		System.out.println(t36(34));
//		
//		System.out.println(t37(3));
//		
//		System.out.println("Sum =" + t38(1,2,3,4,5,6,7,8,9,10));
//		
//		System.out.println("Mult =" + t39(1,2,3,4,5,6,7,8,9,10));
//		
//		System.out.println("Sum.sr =" + t40(1,2,3,4,5,6,7,8,9,10));
//		
//		System.out.println("K vs arraway.first.max index=" + t41(5,1,2,3,4,5,6,7,8,9,10));
//		
//		System.out.println("K vs arraway.last.max index=" + t42(5,1,2,3,4,5,6,7,8,9,10));
//
//		System.out.println("Double in arraway.first.max =" + t43(6.0,5,1,2,3,4,5,6,7,8,9,10));
//
//		System.out.println("List sort arraway without double =" + t44(4,1,2,3,4,5,6,6,70,80,90,100));
//	
//
//		System.out.println("Chars = "+ t45('v',"aaa dd ccccc vvv v ")  );
//		
//		System.out.println("min=" + t46(5,1,2,3,4,5,6,7,8,9,10));
//		
		System.out.println("max1*max2 = " + t50(1,20,2,3,10,10,11));
		
	}

	public static int t27(int a, int b){
		int sum = 0;
		for (int i=a; i < b+1; i++){sum +=  i;}
		return sum;
	}
	
	public static int t28(int a, int b){
		int sum = 1;
		for (int i=a; i < b+1; i++){sum *=  i;}
		return sum;
	}
	
	public static int t29(int a, int b){
		int sum = 0;
		for (int i=a; i < b+1; i++){sum +=  i*i;}
		return sum;
	}
	
	public static int t30(double a, int n){
		int sum = 0; 
		int aa=1;
		for (int i=0; i < n; i++){aa *= (-1) * a; sum += aa; 
//		System.out.println(aa+" " + i+ " " + sum); //list step by step
		}
		return sum;
	}
	
	public static double t31(int a){  // factoria
		double sum = 1;
		for (int i=1; i < a+1; i++){sum *=  i;}
		return sum;
	}
	
	
	public static int[] t32(int a){	// fibonachi list
		int[] aa = new int[a]; 
		aa[0] = 0; 
		aa[1] = 1;
		for (int i=2; i < a; i++){ aa[i] = aa[i-1] + aa[i-2] ;}
		return aa;
	}

	public static String toString(int[] a) {
		String s="";
		for(int i=0; i < a.length ; i++){s += a[i] + " ";}
		return s;
	}
	
	public static int[] t33(double p){	// bank1
		int[] aa = new int[2];
		int k = 0; 			// first month
		double s = 1000; 	//deposit
//		for (int i=0; i <240; i++){ }  
		while (s<=1100){s*=(1 + p/100); k++;}
			aa[0] = k; 
			aa[1] = (int) s;
		return aa;
	}
	
	
	public static void t34(int n) {	// int parser

		int i=1;
		int nn=n;
		
		while (n/i>1){ i*=10;} //�����������
		
		while (i>0) {		
			System.out.println((nn/i) + " ");
			nn=nn - nn / i * i ;
			i/=10; }
	
	}
	
	public static boolean t35(int n){	// simple number
		n=9	;
		
		int i=2;
		while (i<n) { 

			System.out.println(i + " " + n%i);

			if (n%i==0) { return false;}
			i++;
		}
		return true;
		
		}
		
	public static boolean t36(int a){     // fibonachi check
//		a =55;    //0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987, 1597, 2584, 4181, 6765, 10946
		int a0 = 0; 
		int a1 = 1; //-1
		int a2 = 0; //-2  

		do { a0 = a1 + a2 ; 
			if ( a0 ==a ) return true;
			a2 = a1; a1 = a0;			
		} while (a0<= a);
		return false;		
	}
	
	public static String t37(int n){  // 3N+1 for odd\\ N/2 for even

		String s = "N ="+ n;
		while (n!=1) {			
			if (n%2 == 0) n/=2;
			else {n = 3 * n + 1;};		
			s +=   " " + n;	
		}  			
		return s;		
	}
	
	public static int t38(int ...n ){ //sum
		int sum=0;
		for(int i: n) { sum += i; }
		
		return sum;
	}

	public static int t39(int ...n ){ //mult
		int mult=1;
		for(int i: n) { mult *= i; }
		
		return mult;
	}

	public static double t40(int ...n ){ //sum.sr
		int sum = t38(n);
		
		return sum/n.length;
	}

	public static int t41(int k, int ...n ){ //k vs arraway.1st.max
		int ii=0;
		
		for(int i: n) { ii++; if (k<i) return ii; }
//		for(int i: n) { if k  return i+1; }
	
		return 0;
	}

	public static int t42(int k, int ...n ){ //k vs arraway.last.max
		int ii=0;
		int last=0;
		
		for(int i: n) { ii++; if (k<i) last = ii; }
	
		return last;
	}
	
	public static String t43(double b, int k, int ...n ){ //crazy program1
		String s="";

		int ii=t41((int)b,n); 

		for(int i=0; i<n.length;i++ ) { if (ii-1==i) s += " " + b; s += " " +n[i];}
	
		return s;
	}
	
	public static String t44(int k, int ...n ){ //k vs arraway.last.max
		String s="";
		
		int ii=t41(k,n);  //index of arraway
		if (ii==1) s += k + " ";//���������� � < ���� �����
		s +=  n[0] + " ";
//		;
		for(int i=1; i<n.length;i++ ) { 
			if (ii-1==i & k!=n[i-1]) s += k + " ";//�������� � != ��������[i-1] 
			if (n[i-1]!=n[i]) 
				{s +=  n[i] + " " ; 
				
				}
		}
		
		if (ii==0&k>n[n.length-1]) s += k + " "; //���������� � > ���� �����

		
		return s;
	}
	
	
	public static int t45(char c, String s){ 
		int sum = 0;
		char[] cc;
		cc = s.toCharArray();
		for (int i=0 ; i<cc.length; i++){if (cc[i] == c) sum++;} 
		
		return sum;
	}

	public static int t46(int ...n){ // min
		int i = n[0];
		for (int ii:n) if (i>ii) i = ii;
		return i;
	}
	
	
	public static int t47(int ...n){ //sum
		return t38(n);
	}
	
	
	public static int t48(int ...n){ //mult
		return t39(n);
	}
	
	
	public static double t49(int ...n){ //sum.sr
		return t40(n);
	}

	public static int t50(int ...n){ // max
		int i1 = Integer.MIN_VALUE;
		int i2=i1;
		
		for (int ii:n) if (i1<ii) {
		if (i2<ii) {i1=i2; i2=ii;}
			else i1 = ii;}
		return i1*i2;
		}
}
