package Point;

class Point {

	private int x, y;

	Point(){
		setX (0);
		setY (0);
	}

	Point(int x){
		setX (x);
		setY (x);
	}

	Point(int x, int y){
		setX (x);
		setY (y);
	}

	public void setX (int x){
		this.x = x;
	}

	public void setY (int y){
		this.y = y;
	}

	public int getX (){
		return x;
	}

	public int getY (){
		return y;
	}

}