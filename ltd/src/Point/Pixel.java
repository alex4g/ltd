package Point;

class Pixel extends Point{
	private int c;
	
	Pixel(int c){
		setX(c);
		setY(c);		
		setC(c);		
	}
	
	Pixel(){
//		Pixel(0); // ��� ����� ������� ����������� � int 0 ???
		setX(0);
		setY(0);		
		setC(0);	
	}
	

	Pixel(int x, int y, int c){
		setX(x);
		setY(y);		
		setC(c);		
	}
	
	/**
	 * @return the c
	 */
	public int getC() {
		return c;
	}

	/**
	 * @param c the c to set
	 */
	public void setC(int c) {
		this.c = c;
	}
}
