package ltd;

public interface IPayable {
	public double sumSalary(Employee[] employees);
}
