package ltd;

import java.util.Date;

class Manager extends Employee {

	private double bonus;
	
	Manager(){
		
	}
			
	Manager(int i, String n, Date d, double s) {

		super(i, n, d, s);

		setBonus (s * .1);

	} 

	Manager(int i, String n, String sn, double s) {

		super(i, n, sn, s);

		setBonus (10);

	} 
	
	public void setBonus(double bonus) {

		this.bonus = bonus;

	}
	
	public void setBonus(int bonus) {

		this.bonus = super.getSalary() * bonus /100;

	}

	public double getBonus() {
		return bonus;
	}

	public double getSalary() {

		double baseSalary = super.getSalary();

		return baseSalary + bonus;

	}


	
}