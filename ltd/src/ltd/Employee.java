package ltd;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;

class Employee implements IPayable{

	private int inn;
	private String name;
	private String surname;
	private Date hireDate;
	private double salary;
	
	Employee (int i, String n, Date d, double s) {
		this.setInn(i);
		this.setName(n);
		this.setSalary(s);
		this.setHireData(d);
	}
	
	public double sumSalary(Employee[] employees){
		double summEmloyees = 0.0;
		for (int i=0;i<employees.length;i++){
			summEmloyees += employees[i].getSalary();
		}
		return summEmloyees;
	}
	
	Employee (int i, String n, double s) {
//				
		this.setInn(i);
		this.setName(n);
		this.setSalary(s);
//		this.setHireData(d);
	}
	
	Employee (int i, String n, String sn, double s) {
		this.setInn(i);
		this.setName(n);
		this.setSurname(sn);
		this.setSalary(s);
		//this.setHireData(d);
	}
//	int i
	public Employee (){
//		this.setInn(i);
//		this.setName(n);
//		this.setSalary(s);
		
//		this.Employee((randomNum(10)), 
//		(randomChars(randomNum(10)+3) + " " + randomChars(2+randomNum(8))), 1111.0);
//		createEmployee();
	}
	


	public int getInn(){
		return inn;
	}

	public String getName(){
		return name;
	}

	public double getSalary(){
		return salary;
	}

	public Date getHireData(){
		return hireDate;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public void setInn(int inn){
		this.inn = inn;
	}

	public void setName(String name){
		this.name = name;
	}

	public void setSalary(double salary){
		this.salary = salary;
	}

	public void setHireData(Date hireDate){
		this.hireDate = hireDate;
//				 = new Date(); 
//		= hireDate;
	}

	public void generateArrayCust(){
//		this = new  Employee[qty];
//		Employee[] employees = new Employee[qty];
//		for (int i=0; i<this.length; i++) {
//			this = this.createEmployee();
//			employees = employees[i].createEmployee();
//		}
//return employees;
//Employee[] employees = new Employee[qty];
//for (int i=0; i<qty ;i++) {
//	employees[i] = employees[i].createEmployee();
//}
//return employees;
	}
	
	
	public void generateEmployee(){

		this.setInn(randomNum(99999999));
		this.setName(randomChars(randomNum(10)+3));
		this.setSurname(randomChars(randomNum(8)+2));
		this.setSalary((double) randomNum(100000)/100);
		
	}
	

	public static String randomChars(int qty){
		StringBuilder name = new StringBuilder (qty);
		for (int i=0; i<qty ;i++) {
			name.append((char)(randomNum(26)+65));
		}

		 return name.toString();		
	}

	public static int randomNum(int qty){
		return (int)(Math.random()*qty);	
	}

	public String toString() {
		
//		StringBuffer(name.toString() );
		
		return (inn + "\t" + name + " " + surname + "\t" + salary);
//		return (inn + "\t" + name.toString() + " " + surname.toString() + "\t" + salary);
	}
	
	
	public void outStream(String fout){
		String fileName = "employee.txt";
		try(FileWriter output = new FileWriter(fileName);
				BufferedWriter bufOutput= new BufferedWriter(output);
				) {
			String line = fout;
//			while (line != null) {
				// write the line to the output file

				bufOutput.write(line, 0, line.length());
				bufOutput.newLine();
//			}

		} catch (IOException e) { e.printStackTrace(); }
	}

	public void outStream(){
		outStream(this.toString());
	}

	
	public void inStream(String fout){
		String fileName = "employee.txt";
		try(FileWriter output = new FileWriter(fileName);
				BufferedWriter bufOutput= new BufferedWriter(output);
				) {
			String line = fout;
//			while (line != null) {
				// write the line to the output file

				bufOutput.write(line, 0, line.length());
				bufOutput.newLine();
//			}

		} catch (IOException e) { e.printStackTrace(); }
	}
}